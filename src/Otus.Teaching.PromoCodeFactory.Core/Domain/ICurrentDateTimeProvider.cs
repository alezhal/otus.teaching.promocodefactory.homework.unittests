﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    interface ICurrentDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}
