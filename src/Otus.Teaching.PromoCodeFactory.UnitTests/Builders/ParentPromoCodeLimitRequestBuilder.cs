﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    class ParentPromoCodeLimitRequestBuilder
    {
        private int _limit;
        private DateTime _endDate;

        public ParentPromoCodeLimitRequestBuilder WithLimit(int limit)
        {
            _limit = limit;
            return this;
        }
        public ParentPromoCodeLimitRequestBuilder WithEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit
            {
                Limit = _limit,
                EndDate = _endDate
            };
        }

    }
}
