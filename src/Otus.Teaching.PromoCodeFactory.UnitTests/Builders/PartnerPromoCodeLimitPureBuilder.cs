﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class PartnerPromoCodeLimitPureBuilder
    {
        private Guid _id;
        private Guid _partnerId;
        private DateTime _createdDate;
        private DateTime _endDate;
        private DateTime? _cancelDate;
        private int _limit;

        public PartnerPromoCodeLimitPureBuilder()
        {
            //new PartnerPromoCodeLimit
            //{
            //    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
            //    CreateDate = new DateTime(2020, 07, 9),
            //    EndDate = new DateTime(2020, 10, 9),
            //    Limit = 100
            //}
        }

        public PartnerPromoCodeLimitPureBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }
        
        public PartnerPromoCodeLimitPureBuilder WithPartnerId(Guid id)
        {
            _partnerId = id;
            return this;
        }

        public PartnerPromoCodeLimitPureBuilder WithCreatedDate(DateTime createdDate)
        {
            _createdDate = createdDate;
            return this;
        }
        
        public PartnerPromoCodeLimitPureBuilder WithEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }
        
        public PartnerPromoCodeLimitPureBuilder WithCancelDate(DateTime canceDate)
        {
            _cancelDate = canceDate;
            return this;
        }
        public PartnerPromoCodeLimitPureBuilder WithLimit(int limit)
        {
            _limit = limit;
            return this;
        }


        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit
            {
                Id = _id,
                CreateDate = _createdDate,
                EndDate = _endDate,
                CancelDate = _cancelDate,
                Limit = _limit,
                PartnerId = _partnerId
            };
        }
    }
}
