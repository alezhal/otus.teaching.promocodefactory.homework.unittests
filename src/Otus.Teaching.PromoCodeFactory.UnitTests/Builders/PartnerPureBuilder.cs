﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class PartnerPureBuilder
    {
        private Guid _id;
        private string _name;
        private bool _isActive;
        private List<PartnerPromoCodeLimit> _partnerPromoCodeLimits;

        public PartnerPureBuilder()
        {
            //new Partner
            //{
            //    Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
            //    Name = "Суперигрушки",
            //    IsActive = true,
            //    PartnerLimits = new List<PartnerPromoCodeLimit>
            //    {
            //        new PartnerPromoCodeLimit
            //        {
            //            Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
            //            CreateDate = new DateTime(2020,07,9),
            //            EndDate = new DateTime(2020,10,9),
            //            Limit = 100
            //        }
            //    }
            //};

        }

        public PartnerPureBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }
        
        public PartnerPureBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public PartnerPureBuilder WithIsActive(bool isActive)
        {
            _isActive = isActive;
            return this;
        }

        public PartnerPureBuilder WithPartnerPromoCodeLimit(PartnerPromoCodeLimit partnerPromoCodeLimit)
        {
            _partnerPromoCodeLimits = new List<PartnerPromoCodeLimit>();
            _partnerPromoCodeLimits.Add(partnerPromoCodeLimit);
            return this;
        }

        public Partner Build()
        {
            return new Partner
            {
                Id = _id,
                Name = _name,
                IsActive = _isActive,
                PartnerLimits = _partnerPromoCodeLimits
            };
        }
    }
}
