﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public class ParentControllerData_InMemory
    {
        public PartnersController partnerController;
        public IRepository<Partner> partnerRepository;

        public ParentControllerData_InMemory()
        {
            partnerRepository = GetInMemoryRepository();
            partnerController = new PartnersController(partnerRepository);
        }

        private IRepository<Partner> GetInMemoryRepository()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                             .UseInMemoryDatabase(databaseName: "MockDB")
                             .UseLazyLoadingProxies()
                             .Options;

            var initContext = new DataContext(options);

            initContext.Database.EnsureDeleted();

            Seed(initContext);

            var testContext = new DataContext(options);
            var repository = new EfRepository<Partner>(testContext);
           // var repository = new InMemoryRepository<Partner>(testContext);

            return repository;
        }

        public void Seed(DataContext context)
        {
            var partnerPromoCodeLimit = new PartnerPromoCodeLimitPureBuilder()
                 .WithId(Guid.Parse("0e94624b-1ff9-430e-ba8d-ef1e3b77f2d5"))
                 .WithPartnerId(Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319"))
                 .WithLimit(100)
                 .WithCreatedDate(new DateTime(2021, 05, 3))
                 .WithEndDate(new DateTime(2021, 12, 26))
                 .Build();

            var partner = new PartnerPureBuilder()
                .WithId(Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319"))
                .WithName("Каждому кота")
                .WithIsActive(true)
                .WithPartnerPromoCodeLimit(partnerPromoCodeLimit)
                .Build();

            context.Add(partner);
            context.SaveChanges();
        }
    }
}
