﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Net;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        //TODO: Add Unit Tests
        //ИмяЕдиницыТестирования_Условие_ОжидаемыйРезультат
        //Partner
        //PartnerLimit
        //PartnersController:SetPartnerPromoCodeLimitAsync 
        //Task:
        //+ 1. Если партнер не найден, то также нужно выдать ошибку 404;
        //+ 2. Если партнер заблокирован, то есть поле IsActive = false в классе Partner, то также нужно выдать ошибку 400;
        //+ 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        //+ 4. При установке лимита нужно отключить предыдущий лимит;
        //+ 5. Лимит должен быть больше 0;
        //+ 6. Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);

        private readonly IRepository<Partner> _partnersRepositoryInMemory;
        private readonly PartnersController _partnersControllerInMemory;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            #region DataContext in memory
            var contextInMemory = new ParentControllerData_InMemory();
            _partnersRepositoryInMemory = contextInMemory.partnerRepository;
            _partnersControllerInMemory = contextInMemory.partnerController;
            #endregion
        }

        [Fact]
        public async void SetNotActivePartnerPromoCodeLimitAsync_PartnerNotFound_ShouldBeError404()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            var request = new SetPartnerPromoCodeLimitRequest
            { 
                EndDate = DateTime.Now.AddHours(1) ,
                Limit = 15
            };

            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner)null);
            var controller = new PartnersController(mock.Object);

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeOfType<NotFoundObjectResult>();
            result.As<NotFoundObjectResult>().Value.Should().Be("Данный партнер не найден");
        }

        [Fact]
        public async void SetNotActivePartnerPromoCodeLimitAsync_EndDateMayNotBeInPast_ShouldBeError400()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            var request = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 14
            };

        var mock = new Mock<IRepository<Partner>>();
            mock.Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner)null);
            var controller = new PartnersController(mock.Object);

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();
            result.As<BadRequestObjectResult>().Value.Should().Be("Дата не может быть в прошлом");
        }

        [Theory]
        [InlineData("0da65561-cf56-4942-bff2-22f50cf70d43")]
        public async void SetNotActivePartnerPromoCodeLimitAsync_PartnerIsNotActive_ShouldBeError400(string id)
        {
            //Arrange
            var partnerId = Guid.Parse(id);
            var partner = new PartnerPureBuilder()
                .WithId(Guid.Parse(id))
                .WithName("Рыба твоей мечты")
                .WithIsActive(false)
                .Build();
            var request = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = new DateTime(2021, 12, 29),
                Limit = 12
            };

            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var controller = new PartnersController(mock.Object);

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();
            result.As<BadRequestObjectResult>()
                .Value.Should().Be("Данный партнер не активен");
        }


        [Fact]
        public async void SetNotActivePartnerPromoCodeLimitAsync_EnsureNewPartnerPromoCodeLimitSaved_ShouldBe201()
        {
            //Arrange
            var partnerId = Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319");

            var request = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = new DateTime(2021, 12, 29),
                Limit = 10
            };

            //Act
            var result = await _partnersControllerInMemory.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            var okResult = result.Should().BeOfType<CreatedAtActionResult>().Subject;
            okResult.StatusCode.Should().Be(201);
        }
    }
}