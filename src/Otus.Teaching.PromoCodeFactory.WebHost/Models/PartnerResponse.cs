﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PartnerResponse
    {
        public Guid Id { get; set; }

        public bool IsActive { get; set; }
        
        public string Name { get; set; }

        public int NumberIssuedPromoCodes  { get; set; }

        public List<PartnerPromoCodeLimitResponse> PartnerLimits { get; set; }

        public PartnerResponse() { }

        public PartnerResponse(Partner partner)
        {
            Id = partner.Id;
            IsActive = partner.IsActive;
            Name = partner.Name;
            NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            PartnerLimits = partner.PartnerLimits.Select(x => new PartnerPromoCodeLimitResponse
            {
                Id = x.Id,
                PartnerId = x.PartnerId,
                CreateDate = x.CreateDate.ToString(),
                EndDate = x.EndDate.ToString(),
                CancelDate = x.CancelDate.ToString(),
                Limit = x.Limit
            }).ToList();
        }
    }
}